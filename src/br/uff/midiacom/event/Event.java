package br.uff.midiacom.event;

import java.io.Serializable;


@SuppressWarnings("serial")
public class Event implements Serializable {

	private String type;
	private Object data;
	protected boolean remote;
	
	
	public Event(String type) {
		this.type = type;
		remote = false;
	}
	
	
	public Event(String type, Object data) {
		this.type = type;
		this.data = data;
		remote = false;
	}
	
	
	public String getType() {
		return type;
	}
	
	
	public Object getData() {
		return data;
	}
	
	
	public String toString() {
		if (data != null)
			return "[Event:"+ type + " - " + data.toString() + "]";
		else
			return "[Event:"+ type + "]";
	}
}
