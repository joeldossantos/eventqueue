package br.uff.midiacom.event;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;


public class EventQueue extends Thread {

	private HashMap<String, ArrayList<Handler>> handlers;
	private ArrayList<Event> queue;
	private RemoteQueue remote;
	private boolean run;
	
	
	public EventQueue() {
		handlers = new HashMap<String, ArrayList<Handler>>();
		queue = new ArrayList<Event>();
		run = true;
		
		start();
	}
	
	
	public void setRemoteQueue(Socket sock) throws IOException {
		remote = new RemoteQueue(this, sock);
	}
	
	
	public synchronized void register(Object h) {
		for (Method m : h.getClass().getDeclaredMethods()) {
			if (m.isAnnotationPresent(OnEvent.class)) {
				String[] ts = m.getAnnotation(OnEvent.class).value();
				
				for (String t : ts) {
					ArrayList<Handler> l = handlers.get(t);
					if (l == null) {
						l = new ArrayList<Handler>();
						handlers.put(t, l);
					}
					l.add(new Handler(m, h));
				}
			}
		}
	}
    
    
    public synchronized void trigger(Event evt) {
    	queue.add(evt);
        notify();
    }
    
    
    public synchronized void stopQueue() {
    	run = false;
//    	remote.quit();
        notify();
    }
	
	
	@Override
	public void run() {
		while (run || !queue.isEmpty()) {
			try {
				if (queue.isEmpty()) {
                    synchronized (this) { wait(); }
				}
				else{
					Event evt = queue.get(0);
					queue.remove(0);
					
					// send event to handlers
					ArrayList<Handler> l = handlers.get(evt.getType());
					if (l != null) {
						for (Handler h : l)
							h.call(evt);
					}
					
					// send event to remote
					if (remote != null && !evt.remote)
						remote.send(evt);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		remote.quit();
	}
}
