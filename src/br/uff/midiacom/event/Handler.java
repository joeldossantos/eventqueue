package br.uff.midiacom.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class Handler {

	private Method mth;
	private Object obj;
	private Object[] params;
	
	
	protected Handler(Method mth, Object obj) {
		this.mth = mth;
		this.obj = obj;
		this.params = new Object[mth.getParameterTypes().length];
	}
	
	
	protected boolean hasMethod(Method m) {
		return mth.equals(m);
	}
	
	
	protected void call(Event evt)  {
		if (params.length > 0)
			params[0] = evt;
		
		new Thread() {
			@Override
			public void run() {
				try {
					synchronized(obj) { mth.invoke(obj, params); }
				}
				catch (InvocationTargetException e) {
					e.printStackTrace();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}
}
