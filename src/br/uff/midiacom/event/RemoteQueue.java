package br.uff.midiacom.event;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


public class RemoteQueue extends Thread {

	private Socket sock;
	private ObjectOutputStream out;
    private ObjectInputStream in;
    private EventQueue queue;
    private Event event;
	
	
	public RemoteQueue(EventQueue queue, Socket sock) throws IOException {
		this.queue = queue;
		this.sock = sock;
		
		out = new ObjectOutputStream(sock.getOutputStream());
        in = new ObjectInputStream(sock.getInputStream());
        start();
	}
	
	
	protected synchronized void send(Event evt)  {
		event = evt;
		
		try {
			out.writeObject(event);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void quit() {
		try {
			sock.close();
			in.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
    
    @Override
    public void run() {
        while(true){
            try {
            	Event evt = (Event) in.readObject();
                evt.remote = true;
                queue.trigger(evt);
            } 
        	catch (EOFException e) {
            	System.out.println("Stopping RetomeQueue...");
            	quit();
            	return;
            }
        	catch (Exception e) {
        		System.out.println("Exception in RetomeQueue:");
            	e.printStackTrace();
            	System.out.println("Stopping RetomeQueue...");
            	quit();
            	return;
            }
        }
    }
}
